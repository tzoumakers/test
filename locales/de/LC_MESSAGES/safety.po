# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, kosnick
# This file is distributed under the same license as the test package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: test\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-22 21:09+0200\n"
"PO-Revision-Date: 2019-11-22 19:29+0000\n"
"Language-Team: English (https://www.transifex.com/tzoumakers/teams/105065/en/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../safety/index.rst:2
msgid "Ασφάλεια"
msgstr ""

#: ../../safety/index.rst:4
msgid ""
"Εδώ θα περιγράφεται λεπτομερώς οι προϋποθέσεις για την ασφάλεια χρήσης του "
"εργαλείου αλλά και του χρήστη κατά τη λειτουργία."
msgstr ""
