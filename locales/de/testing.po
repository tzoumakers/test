# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2019, kosnick
# This file is distributed under the same license as the test package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: test\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-11-22 21:09+0200\n"
"PO-Revision-Date: 2019-11-22 19:30+0000\n"
"Language-Team: English (https://www.transifex.com/tzoumakers/teams/105065/en/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: en\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../testing/index.rst:2
msgid "Δοκιμή"
msgstr ""

#: ../../testing/index.rst:4
msgid ""
"Εδώ θα περιγράφεται λεπτομερώς όλες οι ενέργεις που έγιναν στο εργαλείο κατά"
" τη φάση της δοκιμής."
msgstr ""

#: ../../testing/index.rst:6
msgid "Στοιχεία που χρειάζονται:"
msgstr ""

#: ../../testing/index.rst:8
msgid "περιγραφή της δοκιμής"
msgstr ""

#: ../../testing/index.rst:9
msgid "συμμετέχοντες (ώστε να μπορούμε να έχουμε ανατροφοδότηση)"
msgstr ""

#: ../../testing/index.rst:10
msgid "συνθήκες δοκιμής"
msgstr ""

#: ../../testing/index.rst:11
msgid "άλλα εργαλεία που απαιτήθηκαν"
msgstr ""

#: ../../testing/index.rst:12
msgid "φωτογραφίες κατά τη δοκιμή"
msgstr ""

#: ../../testing/index.rst:13
msgid "αποτελέσματα"
msgstr ""
