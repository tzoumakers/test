Πρότυπο για τεκμηρίωση της ομάδας Τζουμέικερς
=============================================

.. toctree::
   :maxdepth: 1
   :caption: Πίνακας Περιεχομένων
   :name: sec-general
   
   about/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Απαιτήσεις
   :name: sec-requirements   
   
   requirements/index.rst

.. toctree::
   :maxdepth: 1
   :caption: Σχεδιασμός
   :name: sec-design   
   
   design/index.rst
   
.. toctree::
   :maxdepth: 1
   :caption: Κατασκευή
   :name: sec-development
   
   development/index.rst   
   
.. toctree::
   :maxdepth: 1
   :caption: Δοκιμή
   :name: sec-testing
   
   testing/index.rst      
   
.. toctree::
   :maxdepth: 1
   :caption: Χρήση
   :name: sec-use   
   
   use/index.rst 
   
.. toctree::
   :maxdepth: 1
   :caption: Ασφάλεια
   :name: sec-safety   
   
   safety/index.rst 
   
.. toctree::
   :maxdepth: 1
   :caption: Συντήρηση
   :name: sec-maintenance   
   
   maintenance/index.rst 

.. toctree::
   :maxdepth: 1
   :caption: Αποσυναρμολόγηση
   :name: sec-disassembly   
   
   disassembly/index.rst 
   
.. toctree::
   :maxdepth: 1
   :caption: Άδειες
   :name: sec-licenses   
   
   licenses/index.rst  
   
.. toctree::
   :maxdepth: 1
   :caption: Επικοινωνία
   :name: sec-contact   
   
   contact/index.rst                
   

